#make.file(inputdir=fastq)


make.contigs(processors=3, file=ITS2_IMR230919.file)

sub.sample(fasta=ITS2_IMR230919.trim.contigs.fasta, group=ITS2_IMR230919.contigs.groups)

#Summary.seqs counts the number of sequences you are currently dealing with, running this after every step gives you a good starting point for troubleshooting if the process fails or if it produces something other than what you expect.
summary.seqs(fasta=current)

#Cleaning up obviously bad sequences. I picked 350 bp based on one sample set, it's not as obvious what the cutoff should be for ITS as 16s. May want to us maxhomop too?
screen.seqs(fasta=current, group=current, summary=current, maxambig=0, maxlength=350)

summary.seqs(fasta=current)

#reduce fasta size by only keeping one of each sequence, this generates a names file
unique.seqs(fasta=current)

summary.seqs(fasta=current, name=current)

#replaces both the names and group file (which contain the name of every sequence) with a count table
count.seqs(name=current, group=current)

#pre.cluster to 1% difference to reduce computation time, this step can take a very long time (>4hrs for ~115k sequences)
pre.cluster(fasta=current, diffs=2, count=current)


summary.seqs(fasta=current, count=current)

#chimera.vsearch(processors=3, fasta=current, count=current)


#calls chimeras only from the samples that they are called chimeras, if you want to remove from all samples change dereplicate=f
chimera.vsearch(fasta=current, count=current, dereplicate=t)

#removes chimeras
remove.seqs(fasta=current, accnos=current, count=current)

summary.seqs(fasta=current, count=current)

#RDP classifier using silva as the reference (similar results as RDP reference but some are classified to species). References downloaded from [UNITE_DB](https://unite.ut.ee/repository.php)
classify.seqs(fasta=current, count=current, reference=UNITEv6_sh_dynamic.fasta, taxonomy=UNITEv6_sh_dynamic.tax, cutoff=60)


#remove all non-target sequences
#remove.lineage(fasta=current, count=current, taxonomy=current, taxon=unknown-Protista)

#make tax.summary file for krona that doesn't have above taxon
summary.tax(taxonomy=current, count=current)

#cluster using vsearch, default level is cutoff=0.03, since these are ITS we'll use 5% instead
cluster(fasta=current, count=current, method=agc, processors=4, cutoff=0.05)


summary.seqs(processors=32)

#make OTU matrix 
make.shared(list=current, count=current)

#classify each OTU, used the RDP classification 100% means all seqs in that OTU match at that classification level
classify.otu(list=current, count=current, taxonomy=current)

get.oturep(fasta=current, count=current, list=current, method=abundance)

#check number of sequences in each sample
count.groups(shared=current)

#alpha diversity
summary.single(shared=current, calc=nseqs-sobs-coverage-shannon-shannoneven-invsimpson, subsample=10000)

#beta diversity
dist.shared(shared=current, calc=braycurtis-jest-thetayc, subsample=10000)

#make a rarefied OTU table for heatmaps, indicator species, etc
sub.sample(shared=current, size=10000)
 
#make rarified taxonomy file for krona
sub.sample(taxonomy=ITS2_IMR230919.trim.contigs.good.unique.precluster.pick.UNITEv6_sh_dynamic.wang.pick.taxonomy, count=ITS2_IMR230919.trim.contigs.good.unique.precluster.pick.pick.count_table, list=ITS2_IMR230919.trim.contigs.good.unique.precluster.pick.pick.opti.unique_list.list, size=10000, persample=true)

summary.tax(taxonomy=current, count=current)

system(mkdir send)
system(cp *shared send)
system(cp *cons.tax* send)
system(cp *pick.tax.summary send)
system(cp *.rep.fasta send)
system(cp *lt.ave.dist send)
system(cp *groups.ave-std.summary send)
system(cp mothur.batch send)
system(cp mothur.*.logfile send)


###############
## Krona
#python mothur_krona_XML.py /path/to/*nr_v119_wang.pick.subsample.tax.summary >output.xml
#ktImportXML output.xml