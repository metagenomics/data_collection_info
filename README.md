# OBJETIVO DEL ESTUDIO #
En el marco del financiamiento obtenido para el proyecto Proyecto de Investigación Científica y Tecnológica START UP 2018 (PICT-2017-0217): “Desarrollo de Servicio Biotecnológico para el Estudio del Microbioma en el Viñedo y el Vino”, 
se realizarán muestreos para atender los siguientes obejtivos:

Los objetivos específicos se listan en función de las hipótesis que pretendemos responder y
dentro de los pilares temáticos del proyecto: sanidad, fermentación y terroir.

2.1 SANIDAD
Hipótesis 1: La detección de microorganismos en el suelo y el material vegetal del viñedo
mediante técnicas metagenómicas fomentará la comprensión de la dinámica del
microbioma en el viñedo y permitirá la detección temprana de microorganismos
indeseables.

Objetivo 1.1: Determinar la composición de microorganismos asociados a plantas de Vitis
vinifera cv. Malbec a nivel de rizosfera y filosfera

Objetivo 1.2: Cuantificar las abundancias relativas de los patógenos más comunes de vid:
Erysiphe necator, Plasmopora viticola, Botrytis cinerea, Agrobacterium vitis, Xanthomonas
ampelina, Inocutis jamaicensis, Phaeoacremonium spp., Phaeomoniella chlamydospora y
hongos de la familia Botryosphaeriaceae y Diatrypaceae.

Objetivo 1.3: Evaluar la correlación entre patógenos detectados en rizosfera y filosfera con el
fin de diseñar estrategias de manejo integral que disminuya la presencia de patógenos en el
viñedo.

Hipótesis 2: La composición del microbioma de la rizosfera y filosfera de Vitis vinifera cv.
Malbec está influenciada por las características edafológicas, pedológicas y fisico-químicas
del suelo del viñedo.

Objetivo 2.1: Determinar y comparar la composición del microbioma asociado a plantas de
Vitis vinifera cv. Malbec a nivel de rizosfera y filosfera en parcelas diferentes desde el punto
de vista de la composición físico-química del suelo.

Objetivo 2.1: Evaluar correlación entre patógenos detectados en rizosfera y filosfera.

2.2 FERMENTACIÓN
Hipótesis 3: La composición del microbioma en bayas de Vitis vinifera cv. Malbec, en el
mosto y en etapa de fermentación influye en la calidad del vino elaborado

Objetivo 3.1 Determinar la composición del microbioma en bayas maduras de Vitis vinifera
cv. Malbec

Objetivo 3.2 Determinar la composición del microbioma en los mostos, luego de la cosecha,
estrujado y despalillado, y posteriormente en principio, medio y final de fermentaciones
conducidas en escala piloto en condiciones estandarizadas para las uvas provenientes de las
parcelas seleccionadas de acuerdo a las características fisico-químicas del suelo.

Objetivo 3.3 Determinar el microbioma en vinos una vez finalizadas la fermentación
alcohólica, ya estabilizados y embotellados, y estudiar su correlación con los consorcios
microbianos asociados a uva de cada viñedo y con los detectados en fermentación y la
correlación con los perfiles analíticos y sensorial de los vinos.

2.3 TERROIR
Hipótesis 4: Las propiedades organolépticas de un vino presentan correlación con la
composición microbiana diferencial de una determinada región

Objetivo 4.1 Correlacionar los datos de la composición microbiana a los distintos niveles de
producción con los datos geográficos, microclimáticos y físico químicos de los puntos de
muestreo.

Objetivo 4.2 Correlacionar los perfiles analíticos y sensoriales de los vinos con los datos de la
composición microbiana a las distintas etapas de producción.

# GUIA MUESTREO #

Este repositorio está pensado para administrar la información de muestreo en "El tomillo" como datos de colecta y procesamiento de las muestras, a través de la integración mediante códigos QR. 

## Descarga de app y mapa para trabajar con GPS (sin datos) en su **CELULAR** ##

Para marcar las coordenadas en el mapa se usara [QFIELD](https://play.google.com/store/apps/details?id=ch.opengis.qfield&hl=es_AR) y descargar los 5 archivos en carpeta del [drive](https://drive.google.com/drive/folders/1zWTHDHtPvzFOKDEhpz2U7WM00RLzk7a8?usp=sharing)

## MAPA FINCA EL TOMILLO DE GRUPO PEÑAFLOR ##
![logo_penaflor](https://drive.google.com/uc?export=view&id=13E3LYluN7stdDDxHCzsIgeyTwKEGAKn5)
https://www.google.com/maps/d/u/0/edit?mid=1LxrwNYYlDg6JrkEXMATEw5g8sH9GBN-5&ll=-33.40277905515109%2C-69.21925412821395&z=16
## IMG del mapa con sitios de muestreo ##
![map](https://drive.google.com/uc?export=view&id=13m2vLsThEZ21nibws7ySzN51FiyuH0d_)

## Esquema de muestreo ##
![esquema](https://drive.google.com/uc?export=view&id=1s9TNlckyqC808uZWYG0-qAe5Ltq5DrDF)

### Codificación de muestras ###

Tipo de muestra | Cuartel | Sector | Réplica | Número (fecha) 
--- | --- | --- | ---
S (suelo) | 8 u 11 | A,B o C | 1,2 o 3 | 7.3.19
B (baya) |
R (ritidomis) |
H (hojas) |

#### Ejemplo: ####
muestra ritidomis cuartel 11 D sector C –réplica  2 día 7-3-19): **R11C2_7-7-19**

## Fotos día de muestreo 2019 ##
![IMG0](https://drive.google.com/uc?export=view&id=1JuVctECIS-EVAFuGB2pMq2VG8EyJQQen)
![IMG1](https://drive.google.com/uc?export=view&id=1whL5_Tq55_4xMQUdB45EE4xufGrs2kJZ)
![IMG2](https://drive.google.com/uc?export=view&id=1oia7NAGOmXRT3zYoaPG85634XMueJt4D)
![IMG3](https://drive.google.com/uc?export=view&id=1-dJgc2l7kci3cTlBaJEEf9dQuc0ECm82)
![IMG4](https://drive.google.com/uc?export=view&id=1yJ1jwCmOaQDDMTfSTXLkXZ9ab1Ygj_X9)

## Vinificación 2019 ##

![IMG5](https://drive.google.com/uc?export=view&id=1kafNJRhylKsX1NNdNroYQpjgByZ-Xoyn)

## Prueba de los Vinos cosecha 2019 ##
![IMG6](https://drive.google.com/uc?export=view&id=1slLdk5mLL2ceFN-J_T36CBcX19eQAoff)
![IMG8](https://drive.google.com/uc?export=view&id=1v-9JL7I2q2uy5bpbvZ9mfLj6f8Jv69iv)
## Resultados pruebas secuenciación 2019 ##

### Codificación de muestras ###
código sec ITS |  código sec 16S | código lab | tipo de muestra | características | sitio | fecha | coordeandas | metodo de extracción de ADN | concentración ADN (ng/ul) | A260/280 | A260/230
--- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | ---
sar725 | sar408 | Inc2B_kit | S | Suelo Inculto. Superficial (<40 cm) | Gualtallary_aledaño_viñedo | 27.3.2018 | - | Kit MN Nucleospin DNA Soil (buff SL1+150ul enhancer SLX) + trat RNAsa  | 24,8 | 1,82 | 1,35
sar726 | sar410 | Inc2B_CTAB | S | Suelo Inculto. Superficial (<40 cm) | Gualtallary_aledaño_viñedo | 27.3.2018 | - | CTAB (Protocolo basado en STACH et al., 2001; pero con buffer según Chang et al., 2008) y trat RNAsa | 705,1 | 1,28 | 0,77
sar727 | sar411 | Up30mg | B | Bayas lavadas con Sol. Isotónica con agitación por 3 hrs. Colecta por centrifugación. 30 mg de pellet | Gualtallary_Malbec_15años | 27.3.2018 | - | CTAB (Protocolo basado en STACH et al., 2001; pero con buffer según Chang et al., 2008) y trat RNAsa | 14,7 | 1,45 | 0,25
sar728 | sar412 | Rp_150mg | R | Ritidomis lavadas con Sol. Isotónica con agitación por 3 hrs. Colecta por centrifugación. 150 mg de pellet | Gualtallary_Malbec_15años | 27.3.2018 | - | CTAB (Protocolo basado en STACH et al., 2001; pero con buffer según Chang et al., 2008) y trat RNAsa | 177,6 | 1,32	| 0,62
sar729 | - | Vp_90mg | V | Muestra de vinificación (etapa final_8CFF_21032019) lavadas con Sol. Isotónica con agitación por 3 hrs. Colecta por centrifugación. 90 mg de pellet | Gualtallary_Malbec_2años | 21.3.2019 | - |  CTAB (Protocolo basado en STACH et al., 2001; pero con buffer según Chang et al., 2008) y trat RNAsa | 56,6 | 1,56 | 0,42
sar730 | sar417 | R2_30mg | R | Ritidomis molida con bead beater. 30 mg | Gualtallary_Malbec_15años | 27.3.2018 | - | CTAB (Protocolo basado en STACH et al., 2001; pero con buffer según Chang et al., 2008) y trat RNAsa | 138,6 | 1,3 | 0,55
sar731 | sar419 | H2_40mg | H | Hojas molida con bead beater. 40 mg | Gualtallary_Malbec_15años | 27.3.2018 | - | CTAB (Protocolo basado en STACH et al., 2001; pero con buffer según Chang et al., 2008) y trat RNAsa | 58,5 | 1,19 | 0,43
sar732 | sar415 | U2_60mg | B | Piel de uva molida con bead beater. 60 mg | Gualtallary_Malbec_15años | 27.3.2018 | - | CTAB (Protocolo basado en STACH et al., 2001; pero con buffer según Chang et al., 2008) y trat RNAsa | 9,2 | 1,68 | 0,17
sar733 | - | H1_100mg | H | Hojas molida con bead beater. 100 mg | Gualtallary_Malbec_15años | 27.3.2018 | - | CTAB (Protocolo basado en STACH et al., 2001; pero con buffer según Chang et al., 2008) y trat RNAsa | 215,4 | 1,3 | 0,57

### Gráficos abundancia relativa (Krona) ###
[Procariotas_16S](https://drive.google.com/uc?id=1AL9-COWbKZ95p72QM5-9CNb4NMHWh_a0) | [Hongos_ITS](https://drive.google.com/uc?id=1SoKkJCPBdCAG7nnjKYYWN67rsomBQ_1M)

### Para explorar más profundo los datos con Phinch ###
Descargar la App de acuerdo al sistema operativo con el que trabaja: [para_Windows](https://github.com/PhinchApp/Phinch/releases/download/v2.0.1/Windows.Phinch2.Setup.2.0.1.exe), [para_Linux](https://github.com/PhinchApp/Phinch/releases/download/v2.0.1/Linux.Phinch-2.0.1-x86_64.AppImage) o [para_Mac](https://github.com/PhinchApp/Phinch/releases/download/v2.0.1/macOS.Phinch2-2.0.1.dmg)

Una vez con la App, descargar los archivos biom que se utilizarán en Phinch:
[Procariotas_16S](https://drive.google.com/uc?id=108XHMcqNDJi-AsKsGKOxxpguRtaX8gha) | [Hongos_ITS](https://drive.google.com/uc?id=1vQ1H6ERug_wp5T655GfXKVQoNXvV41Z4)

## Fotos día de muestreo 2020 ##
![IMG9](https://drive.google.com/uc?export=view&id=1JmBa4uTkA1vTPNmBXuFKuwUBGF7PnAL2)
![IMG10](https://drive.google.com/uc?export=view&id=1EMHDyhP01a0dcL5vhAs0eTDKl3R0JezF)
![IMG11](https://drive.google.com/uc?export=view&id=10zhVXuDTKrA20cS22Mx9r89HX6agxqkS)
![IMG12](https://drive.google.com/uc?export=view&id=1mQM1xArWi3LlvzRqbBKHeTBRzas7_9xU)
![IMG13](https://drive.google.com/uc?export=view&id=1lim77A9LPK1nLDzZ2qOT_SeoJLc5P4vX)
![IMG14](https://drive.google.com/uc?export=view&id=1MDmvvBLlMJWzDMdiXVqiIhantvZEC-_4)
![IMG15](https://drive.google.com/uc?export=view&id=1bImDqbXF5cOH_A4cSaPipmUAUTlLxi5t)
![IMG16](https://drive.google.com/uc?export=view&id=1AJMPdnbZY12Tt4K4pMflRerW-W3l8ing)

## Vinificación 2020 ##
![IMG17](https://drive.google.com/uc?export=view&id=1PdJa3MSOqfz4Hbty5Id-duh6MHzpnTMK)
![IMG18](https://drive.google.com/uc?export=view&id=1gnf-31m-NHoLHf4QJKSjVs87FjNEquXL)

## Fotos día de cosecha y muestreo 2021 ##
![IMG19](https://drive.google.com/uc?export=view&id=1lT3abQ7QXZNeu24pI71V6s_wruh5kDX6)
![IMG20](https://drive.google.com/uc?export=view&id=1kxPAXgE1D8OV6LkyNRcOLlReB4hUuk10)
![IMG21](https://drive.google.com/uc?export=view&id=1lb40VnAQnnoaynmPAFM2Ok6qeyBasgSO)
![IMG22](https://drive.google.com/uc?export=view&id=1kvl9tq2DrXmYulUBAvpx5VGRY-Ib5Pot)
![IMG23](https://drive.google.com/uc?export=view&id=1lIHrgsSI8Kbm-C-X7rAQE2JXFRfsRYjF)
![gif_mosto](https://drive.google.com/uc?export=view&id=1q8XT_iKCcHVCO5v4c041NDXD8PDqMgx3)

# MUESTRAS #

## **[8A1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8A1_7.3.19.md)** ##
## EEA_INTA_Mendoza ##
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
538f97cf-e25d-4f31-86e5-216618845117
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/77dbda7ee146b866cfa5d511f690f39809445aa5/8A1_7.3.19.md&backcolor=%23ffffff)

### 2019 ###

Tipo de muestra | Código | Coordenadas GPS | Observaciones
--- | --- | --- | ---
Suelo | S8A1_7.3.19 | -33.40301, -69.22058 | ---
Baya | B8A1_7.3.19 | -33.40301, -69.22058 | ---
Ritidomis | R8A1_7.3.19 | -33.40301, -69.22058 | ---
Hojas | H8A1_7.3.19 | -33.40301, -69.22058 | ---
Vinificacion | V8A_19.2.20 | NC | En este caso se juntan los sectores 1,2y3 y se toman muestras a t0, t0.5 y t1 de acuerdo a la densidad

### 2020 ###

Tipo de muestra | Código | Coordenadas GPS | Observaciones
--- | --- | --- | ---
Suelo | S8A1_19.2.20 | -33.40301, -69.22058 | ---
Baya | B8A1_19.2.20 | -33.40301, -69.22058 | ---
Ritidomis | R8A1_19.2.20 | -33.40301, -69.22058 | ---
Vinificacion | V8A_19.2.20 | NC | En este caso se juntan los sectores 1,2y3 y se toman muestras a t0, t0.5 y t1 de acuerdo a la densidad


### **[8A2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8A2_7.3.19.md)** ###
# EEA_INTA_Mendoza #
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
98cef78a-6e67-4aee-91e2-576cd6eb3026
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8A2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8A2_7.3.19 | --- | --- | ---
Baya | B8A2_7.3.19 | --- | --- | ---
Ritidomis | R8A2_7.3.19 | --- | --- | ---
Hojas | H8A2_7.3.19 | --- | --- | ---

###**[8A3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8A3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
2d8d736d-229f-40f2-9e6f-33ca59258b5c
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8A3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8A3_7.3.19 | --- | --- | ---
Baya | B8A3_7.3.19 | --- | --- | ---
Ritidomis | R8A3_7.3.19 | --- | --- | ---
Hojas | H8A3_7.3.19 | --- | --- | ---


###**[8B1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8B1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
12dfdf7d-6043-45dd-b784-20d4fa6e5838
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8B1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8B1_7.3.19 | --- | --- | ---
Baya | B8B1_7.3.19 | --- | --- | ---
Ritidomis | R8B1_7.3.19 | --- | --- | ---
Hojas | H8B1_7.3.19 | --- | --- | ---

###**[8B2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8B2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
7a6a9a7c-8f8a-4258-acf6-1b03d57f0bca
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8B2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8B2_7.3.19 | --- | --- | ---
Baya | B8B2_7.3.19 | --- | --- | ---
Ritidomis | R8B2_7.3.19 | --- | --- | ---
Hojas | H8B2_7.3.19 | --- | --- | ---

###**[8B3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8B3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
cf2f8cf7-c587-47fa-ab6b-7c356a3a687d
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8B3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8B3_7.3.19 | --- | --- | ---
Baya | B8B3_7.3.19 | --- | --- | ---
Ritidomis | R8B3_7.3.19 | --- | --- | ---
Hojas | H8B3_7.3.19 | --- | --- | ---

###**[8C1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8C1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
11087c8f-94d0-4e76-ab7b-424ba2e0f8b9
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8C1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8C1_7.3.19 | --- | --- | ---
Baya | B8C1_7.3.19 | --- | --- | ---
Ritidomis | R8C1_7.3.19 | --- | --- | ---
Hojas | H8C1_7.3.19 | --- | --- | ---

###**[8C2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8C2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
27b7bf92-642f-4727-8e17-28cbf2e312ed
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8C2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8C2_7.3.19 | --- | --- | ---
Baya | B8C2_7.3.19 | --- | --- | ---
Ritidomis | R8C2_7.3.19 | --- | --- | ---
Hojas | H8C2_7.3.19 | --- | --- | ---

###**[8C3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8C3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
4e3a11bd-2b44-4df4-a0e6-e2c5afab8481
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8C3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8C3_7.3.19 | --- | --- | ---
Baya | B8C3_7.3.19 | --- | --- | ---
Ritidomis | R8C3_7.3.19 | --- | --- | ---
Hojas | H8C3_7.3.19 | --- | --- | ---

##suelo con roca en superficie

###**[11A1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11A1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
ef27075a-1431-4788-bc19-9e3532b8fcef
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11A1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11A1_7.3.19 | --- | --- | ---
Baya | B11A1_7.3.19 | --- | --- | ---
Ritidomis | R11A1_7.3.19 | --- | --- | ---
Hojas | H11A1_7.3.19 | --- | --- | ---

###**[11A2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11A2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
60552311-f6c0-44aa-9f29-d6e682343052
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11A2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11A2_7.3.19 | --- | --- | ---
Baya | B11A2_7.3.19 | --- | --- | ---
Ritidomis | R11A2_7.3.19 | --- | --- | ---
Hojas | H11A2_7.3.19 | --- | --- | ---

###**[11A3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11A3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
f125ba1d-a1dc-4fff-abf1-a59d6d6e52c1
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11A3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11A3_7.3.19 | --- | --- | ---
Baya | B11A3_7.3.19 | --- | --- | ---
Ritidomis | R11A3_7.3.19 | --- | --- | ---
Hojas | H11A3_7.3.19 | --- | --- | ---

###**[11B1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11B1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
2702d1bd-d651-4ade-b6c2-23db4f5acbf9
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11B1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11B1_7.3.19 | --- | --- | ---
Baya | B11B1_7.3.19 | --- | --- | ---
Ritidomis | R11B1_7.3.19 | --- | --- | ---
Hojas | H11B1_7.3.19 | --- | --- | ---

###**[11B2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11B2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
55568c73-1e6b-47c9-bd0b-270dd9923575
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11B2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11B2_7.3.19 | --- | --- | ---
Baya | B11B2_7.3.19 | --- | --- | ---
Ritidomis | R11B2_7.3.19 | --- | --- | ---
Hojas | H11B2_7.3.19 | --- | --- | ---

###**[11B3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11B3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
41ca99b6-86c4-40bd-a7e7-b83831da2edb
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11B3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11B3_7.3.19 | --- | --- | ---
Baya | B11B3_7.3.19 | --- | --- | ---
Ritidomis | R11B3_7.3.19 | --- | --- | ---
Hojas | H11B3_7.3.19 | --- | --- | ---

###**[11C1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11C1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
1327c45f-606b-4972-aec6-3ab337bf55bc
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11C1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11C1_7.3.19 | --- | --- | ---
Baya | B11C1_7.3.19 | --- | --- | ---
Ritidomis | R11C1_7.3.19 | --- | --- | ---
Hojas | H11C1_7.3.19 | --- | --- | ---

###**[11C2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11C2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
3d76aedf-ca38-49fb-92e7-1e083686d4af
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11C2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11C2_7.3.19 | --- | --- | ---
Baya | B11C2_7.3.19 | --- | --- | ---
Ritidomis | R11C2_7.3.19 | --- | --- | ---
Hojas | H11C2_7.3.19 | --- | --- | ---

###**[11C3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11C3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
62c85a77-0b29-4299-9e3f-3c97e1c5e394
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11C3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11C3_7.3.19 | --- | --- | ---
Baya | B11C3_7.3.19 | --- | --- | ---
Ritidomis | R11C3_7.3.19 | --- | --- | ---
Hojas | H11C3_7.3.19 | --- | --- | ---



