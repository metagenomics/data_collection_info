#PREPARAR REFERENCIA
#Preparation of silva reference v132 optimized for region V4-V5 (primers 515FB/926R) downloaded from https://mothur.org/w/images/3/32/Silva.nr_v132.tgz , then tar zxvf Silva.nr_v132.tgz
#pcr.seqs(fasta=ecoli_16S.fasta, oligos=oligos_seqs_4_V4_V5)
#align.seqs(fasta=ecoli_16S.pcr.fasta, reference=/home/quetjaune/Documents/PICT_STARTUP/extraccionADN_PCR_secuenciacion/Secuenciacion_7mtrasejemplo_16S_05092019/silva.nr_v132.align, processors=6)
#summary.seqs(fasta=ecoli_16S.pcr.align) #Esto me da las posiciones para recortar los extremos en el alineamiento: 13862 a 27654
#summary.seqs(fasta=/home/quetjaune/Documents/PICT_STARTUP/extraccionADN_PCR_secuenciacion/Secuenciacion_7mtrasejemplo_16S_05092019/silva.nr_v132.align)
pcr.seqs(fasta=/home/quetjaune/Documents/PICT_STARTUP/extraccionADN_PCR_secuenciacion/Secuenciacion_7mtrasejemplo_16S_05092019/silva.nr_v132.align, start=13862, end=27654, keepdots=F, processors=3) #esto es para recortar el archivo de ref
erencia a las posiciones optimas
summary.seqs(fasta=silva.nr_v132.pcr.align)