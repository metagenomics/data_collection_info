#Ejemplo primera secuenciación rRNA 16S bacterias en INTA Castelar: Archivo 'SAR09-2019 MS-SAR30-2019.7z' recibido el 04092019
#descomprimir
```
gunzip sar*gz
```
#preparar barcode file
```
grep -B1 "+" *I[1-2]* | grep -v "+" | sort -u | tail -14 | cut -f1,3 -d '-' | sed 's/-/\t/' > barcodes_x_samples
```
#luego modifique con nano para tener este formato: 'barcode barcodeString sampleName'
#arme el archivo file con la info de las muestras junto con el codigo_nombre de cada una
```
paste <(ls -lth sar4*R1*fastq | sed 's/ /\t/g' | cut -f9-10) <(ls -lth sar4*R2*fastq | sed 's/ /\t/g' | cut -f9-10) > file_4_input
paste <(ls -lth sar4*I1*fastq | sed 's/ /\t/g' | cut -f9-10) <(ls -lth sar4*I2*fastq | sed 's/ /\t/g' | cut -f9-10) > index_4_input
paste <(cat file_4_input) <(cat index_4_input) > 16S_exam_040919
```
#y lo terminé de editar con nano

#para crear contigs en mothur v.1.36.1

```
mothur > make.contigs(file=16S_exam_040919, oligos=barcodes_x_samples)
```
#Run mothur v1.36.1 in batch mode using the mothur.c file but first prepare the references (first lines in mothur.c)
```
/usr/bin/mothur mothur.c
```


