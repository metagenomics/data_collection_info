Descargar Paolinelli16S.zip y PaolinelliITS.zip files from link in dropbox provided by [IMR Sequencing facility](https://imr.bio/) and backup in [Drive](https://drive.google.com/drive/folders/1pTnYk-g9Z0dvn1hj_2i6Pmf6RZvfmfmo?usp=sharing)

Descomprimir y preparar archivos para analisis con mothur

```
ls *R1*fastq | sort > R1_names
ls *R2*fastq | sort > R2_names
cut -d "_" -f1 R1_names > codes
paste codes R1_names R2_names > 16S_STE1580_230919.files
```
Luego es necesario preparar la referencia de silva_v132 para que quede optimizada a la región amplificada V4_V5 mediante el archivo [V4V5prep_ref_4_mothur.c](https://bitbucket.org/metagenomics/data_collection_info/src/master/V4V5_prep_ref_4_mothur.c)
```
/usr/bin/mothur V4V5_prep_ref_4_mothur.c
```
Finalmente correr el análisis completo usando el archivo [V4V5_mothur.c](https://bitbucket.org/metagenomics/data_collection_info/src/master/V4V5_mothur.c)
```
/usr/bin/mothur V4V5_mothur.c
```
Para visualizar resultados en Phinch, necesitamos construir un archivo biom
```
mothur '#make.biom(shared=16S_exam_040919.trim.contigs.good.unique.good.filter.unique.precluster.abund.pick.pick.phylip.an.unique_list.shared, constaxonomy=16S_exam_040919.trim.contigs.good.unique.good.filter.unique.precluster.abund.pick.pick.phylip.an.unique_list.0.01.cons.taxonomy, metadata=metadata_4_biom)'
```
