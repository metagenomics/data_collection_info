#PREPARAR REFERENCIA
#Preparation of silva reference v132 optimized for region V3-V4 (primers 341F/805R) downloaded from https://mothur.org/w/images/3/32/Silva.nr_v132.tgz , then tar zxvf Silva.nr_v132.tgz
#pcr.seqs(fasta=ecoli_16S.fasta, oligos=oligos_seqs_4_V3_V4)
#align.seqs(fasta=ecoli_16S.pcr.fasta, reference=silva.nr_v132.align, processors=6)
#summary.seqs(fasta=ecoli_16S.pcr.align) #Esto me da las posiciones para recortar los extremos en el alineamiento: 6428 a 23440
#summary.seqs(fasta=silva.nr_v132.align)
#pcr.seqs(fasta=silva.nr_v132.align, start=6400, end=23500, keepdots=F, processors=6) #esto es para recortar el archivo de referencia a las posiciones �ptimas
#summary.seqs(fasta=silva.nr_v132.pcr.align)
#CREAR CONTIGS
#make.contigs(file=16S_exam_040919, oligos=barcodes_x_samples, processors=6)
#summary.seqs(fasta=16S_exam_040919trim.contigs.fasta)
#FIRST STAGE
sub.sample(fasta=16S_exam_040919trim.contigs.fasta, group=16S_exam_040919contigs.groups)
summary.seqs(processors=6)
screen.seqs(fasta=current, group=current, maxambig=0, minlength=440, maxlength=467, processors=3)
summary.seqs(processors=6)
unique.seqs()
summary.seqs(processors=6)
count.seqs(name=current, group=current, processors=3)
summary.seqs(processors=3)
align.seqs(fasta=current, reference=silva.nr_v132.align, flip=T, processors=3)
summary.seqs(processors=6)
#SECOND STAGE
screen.seqs(fasta=current, count=current, summary=current, start=6388, end=25316, maxhomop=8, processors=3)
summary.seqs(processors=6)
filter.seqs(fasta=current, trump=., vertical=T, processors=3)
summary.seqs(processors=6)
unique.seqs(fasta=current, count=current)
summary.seqs(processors=6)
pre.cluster(fasta=current, count=current, diffs=2, processors=3)
summary.seqs(processors=6)
chimera.uchime(fasta=current, count=current, dereplicate=t, processors=3)
summary.seqs(processors=6)
remove.seqs(fasta=current, accnos=current)
summary.seqs(processors=6)
classify.seqs(fasta=current, count=current, reference=silva.nr_v132.align, taxonomy=silva.nr_v132.tax, iters=1000, cutoff=80, processors=3)
summary.seqs(processors=6)
remove.lineage(fasta=current, count=current, taxonomy=current, taxon=Chloroplast-Mitochondria-unknown)
#remove.groups(count=current, fasta=current, taxonomy=current, groups=Mock)
cluster.split(fasta=current, count=current, taxonomy=current, splitmethod=classify, taxlevel=6, processors=3)
#an alternative to cluster.split is to calculate distances and then get the clusters)
#dist.seqs(fasta=current, cutoff=0.20)
#cluster(column=current#dist, count=current)
#dist.seqs(fasta=stability.trim.contigs.good.unique.good.filter.unique.precluster.pick.pick.pick.fasta, output=lt, processors=8)
#clearcut(phylip=stability.trim.contigs.good.unique.good.filter.unique.precluster.pick.pick.pick.phylip.dist)
#summary.seqs(processors=8)
make.shared(list=current, count=current, label=0.03)
classify.otu(list=current, count=current, taxonomy=current, label=0.03)
phylotype(taxonomy=current)
make.shared(list=current, count=current, label=1)
classify.otu(list=current, count=current, taxonomy=current, label=1)
#make a phylogenetic tree
#dist.seqs(fasta=FILE.trim.contigs.good.unique.good.filter.unique.precluster.pick.pick.pick.fasta, output=lt, processors=8)
#clearcut(phylip=FILE.trim.contigs.good.unique.good.filter.unique.precluster.pick.pick.pick.phylip.dist)
tree.shared(shared=current) #unique_list.shared
#alpha diversity
#phylo.diversity(tree=r01072016pe.trim.contigs.good.unique.good.filter.unique.precluster.pick.pick.pick.phylip.tre, count=r16042016rc.trim.contigs.good.unique.good.filter.unique.precluster.denovo.uchime.pick.pick.pick.count_table, rarefy=
T)
rarefaction.single(shared=current)